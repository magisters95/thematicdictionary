package com.magister.controller;

import com.magister.entity.Word;
import com.magister.repository.WordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/words")
public class WordController {
    @Autowired
    private WordRepository wordRepository;

    @GetMapping(path = "/add")
    public String addNewWord(@RequestParam String name) {
        Word newWord = new Word();
        newWord.setName(name);
        wordRepository.save(newWord);
        return "OK";
    }

    @GetMapping(path = "/all")
    public Iterable<Word> getAllUsers() {
        return wordRepository.findAll();
    }
}